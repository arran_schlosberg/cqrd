<?php

namespace Oonix\Cqrd\Types;

class String extends TypeHint {

	protected $_string_cast_ok = true; //duuh
	
	protected function typeCheckAndCast($val){
		return array(
			is_scalar($val),
			(string) $val
		);
	}

}

?>
