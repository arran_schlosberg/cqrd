<?php

namespace Oonix\Cqrd\Types;

class Number extends TypeHint {

	protected $_string_cast_ok = true;
	
	protected function typeCheckAndCast($val){
		$cast = false;
		switch(gettype($val)){
			case 'array':
				$cast = count($val);
				break;
			case 'integer': case 'double':
				$cast = $val;
				break;
			case 'string':
				if(is_numeric($val)){
					$cast = strpos($val, '.')===false ? (int) $val : (float) $val;
				}
				break;
		}
		
		return array($cast!==false, $cast);
	}

}

?>
