<?php

namespace Oonix\Cqrd;

class Klein extends \Klein\Klein {

	/**
	 * Instance of PhpRbac
	 */
	private $_rbac;
	
	private $_userId = null;
	
	/**
	 * Cqrd uses the respondIfNotPermission() magic function to enforce authorisation rules.
	 * If an app uses respondPrepend() it bypasses these by having a route processed before the Cqrd authorisation.
	 * Once all initialisation has been performed by Cqrd, this value is set, and respondPrepend() / respondAfter() do not bypass authorisation.
	 */
	private $_endInitCount = null;

	/**
	 * Works identically to Klein::respond() except that a condition boolean and a PhpRbac permission arguments.
	 * Condition and permission are shifted off the argument array and checked to match, if it passes then the Klein response is registered.
	 */
	private function conditionalResponse(){
		$args = func_get_args();
		$condition = array_shift($args);
		$perm = array_shift($args);
		if(Cqrd::s()->rbacCheck($perm)===$condition){
			call_user_func_array(array($this, 'respond'), $args);
		}
	}
	
	/**
	 * Determine which condition should be met with regards to RBAC permission checking of conditionalResponse()
	 */
	public function __call($func, $args){
		switch($func){
			case 'respondIfPermission':
				$condition = true;
				break;
			case 'respondIfNotPermission':
				$condition = false;
				break;
			default:
				throw new CqrdException("No extended Klein function named '{$func}' exists.");
		}
		array_unshift($args, $condition);
		call_user_func_array(array($this, 'conditionalResponse'), $args);
	}
	
	public function endInit(){
		if(!is_null($this->_endInitCount)){
			throw new CqrdException("Attempt to set Klein value endInitCount once already set. This may create a security vulnerability.");
		}
		else {
			$this->_endInitCount = $this->routes->count();
		}
	}
	
	/**
	 * Create a route as with respond() but place it in an arbitrary position within the route collection.
	 * Accepts argument as per Klein::respond() but with a prepended integer >= 0 for use with array_slice()
	 * The prepended integer is added to the value of $_endInitCount, see its comments for an explanation.
	 */
	public function respondAfter(){
		if(is_null($this->_endInitCount)){
			throw new CqrdException("Prepending Klein routes can only occur after calling endInit(). It is expected that this is done by Cqrd::init().");
		}
		$args = func_get_args();
		$pos = $this->_endInitCount + ((int) array_shift($args));
		$route = call_user_func_array(array($this, 'respond'), $args);
		$this->routes->popAndSlice($pos); //we are using \Oonix\Cqrd\RouteCollection with this extended method
		return $route;
	}
	
	/**
	 * Convenience wrapper for ::respondAfter(0...)
	 */
	public function respondPrepend(){
		$args = func_get_args();
		array_unshift($args, 0);
		return call_user_func_array(array($this, 'respondAfter'), $args);
	}

}
