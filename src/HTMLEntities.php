<?php

namespace Oonix\Cqrd;

class HTMLEntities {
	private $_data;

	public function __construct($data){
		if(!is_array($data)){
			throw new CqrdException("HTMLEntities expects an array of values.");
		}
		$this->_data = $data;
	}
	
	public function __get($key){
		$val = &$this->_data[$key];
		if(!isset($val)){
			return null;
		}
		else if(is_array($val)){
			return new HTMLEntities($val);
		}
		else {
			return htmlentities($val);
		}
	}
}

?>
