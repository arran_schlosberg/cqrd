<?php

namespace Oonix\Cqrd;

class ServiceProvider extends \Klein\ServiceProvider {

	private $_rendering = false;

	/**
	 * Override standard behaviour to additionally track whether or not we are currently rendering such that Paris models can automatically implement XSS protection
	 */
	public function render($view, array $data = array()){
		$was = $this->_rendering;
		$this->_rendering = true;
		$data['Cqrd'] = $data['C'] = Cqrd::s();
		parent::render($view, $data);
		$this->_rendering = false;
	}
	
	/**
	 * Public "getter" for the _rendering flag
	 */
	public function rendering(){
		return $this->_rendering!==false;
	}
	
	/**
	 * Convenience wrapper to return sharedData()->cqrdData[] or an empty array if it is not set
	 */
	public function cqrd(){
		return isset($this->sharedData()->cqrdData) ? $this->sharedData->cqrdData : array();
	}
}
?>
