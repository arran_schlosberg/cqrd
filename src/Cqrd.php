<?php

namespace Oonix\Cqrd;

use Oonix\Encryption\Sessions\EncryptedFileSession;
use Oonix\Cqrd\Types\String;

class Cqrd {

	private static $_singleton = null;
	private $_entropy = null;
	private $_klein = null;
	private $_mongo = null;
	private $_user = null;
	private $_session_encrypted = false;
	private $_rbac = null;

	/**
	 * Default configuration directives, overriding values are passed to Cqrd::init() as the first parameter
	 */
	private $_config = array(
		"env" => "prod", // prod / dev
		"debug" => false, // must be explicitly set to boolean true AND be in the dev environment
		"entropy" => array( //used as HMAC secret key for derivation of various seeds / peppers that need to remain constant across requests
			"type" => "file", //file or raw
			"src" => "cqrd-entropy", //source-file or raw entropy
			"file_use_include_path" => true, //see http://php.net/manual/en/function.file-get-contents.php
			"hmac_algo" => "sha512" //default algorithm for use in the HMAC
		),
		"session" => array( //encrypted session handler
			"cipher" => "AES-256-CBC", //method for encryption of session data
			"hmac_algo" => "sha512", //hash for derivation of encryption key and save path, using the session_id as a key
			"ini" => array( //PHP runtime config session.*
				"entropy_file" => "/dev/urandom",
				"entropy_length" => 512,
				"hash_function" => "sha512"
			)
		),
		"https" => array(
			"require" => true, //anything except for explicit boolean false will be considered true
			"domain" => null //must be set if require = true
		),
		"klein" => array(
			"views" => array(
				"dir" => "./views/",
				"default" => array( //see response config option
					"header" => "header",
					"footer" => "footer",
					"login" => "login",
					"403" => "403",
					"404" => "404"
				)
			),
			"responses" => array( //default functions for standard responses, see the constructor for their definitions
				"header" => null,
				"footer" => null
			)
		),
		"phpids" => array(
			"ini" => "PHPIDS.ini",
			"limits" => array(
				"high" => 10, //deemed as high impact in the logs
				"abort" => 30, //abort Klein processing and render the abort_view
				"abort_view" => "phpids"
			)
		),
		"mongo" => array(
			"conn" => array( //see MongoClient::__construct for details
				"server" => "mongodb://localhost:27017",
				"options" => array("connect" => true),
				"driver_options" => array()
			),
			"namespace" => "oonix"
		),
		"users" => array(
			"password" => array(
				"hash" => array( //PBKDF2 is used to generate the passphrase to this user's private key; this is also used as the key for the HMAC of the username which is used in password authentication
					"min_iterations" => 65536,
					"algo" => "sha256",
					"salt_len" => 16
				),
				"strength" => array( //must pass both regex and callback; set either to null to skip
					"regex" => "/^(?=.*[A-z])(?=.*[\d])(?=.*[^A-z\d]).{8,}$/",
					"callback" => null, //callable as passed to call_user_func() - accepts one parameter (the password) and the result is cast as boolean
					"desc" => "At least 8 characters, including at least one letter, one number, and one non-alphanumeric character."
				)
			),
			"pki" => array(
				"configargs" => array( //see http://php.net/manual/en/function.openssl-csr-new.php for details
					"private_key_type" => OPENSSL_KEYTYPE_RSA,
					"private_key_bits" => 4096,
				),
				"symmetric" => array( //assymetric encryption is used purely to store the randomly generated symmetric key; authenticated symmetric encryption incorporates CQRD entropy to ensure data entry via the application
					"cipher" => "AES-256-CBC",
					"hmac" => "sha512",
					"cipher_key_len" => 32, //bytes
					"hmac_key_len" => 32
				)
			)
		),
		"csrf" => array(
			"bytes" => 32 //length of the session-specific key
		)
	);

	/**
	 * Singleton patter with a private constructor, only deals with configuration.
	 */
	private function __construct(array $config = array()){
		$this->_config = array_replace_recursive($this->_config, $config);

		//debugging
		$this->_config['debug'] = $this->_config['debug']===true && $this->_config['env']==='dev';
		ini_set('display_errors', $this->_config['debug']);
		
		if($this->_config['https']['require']!==false && empty($this->_config['https']['domain'])){
			throw new Exception("A secure domain must be supplied unless the https/require config directive is explicitly set to boolean false.");
		}
		
		//set anonymous functions for the default header / footer responses
		$resp = &$this->_config['klein']['responses'];
		foreach(array('header', 'footer') as $view){
			if(is_null($resp[$view])){
				$resp[$view] = function($req, $resp, $service) use ($view) {
					$conf = \Oonix\Cqrd\Cqrd::s()->config('klein/views');
					$service->render("{$conf['dir']}/{$conf['default'][$view]}.phtml");
				};
			}
		}
	}
	
	/**
	 * Initialisation function which additionally acts as a singleton
	 * Route permissions are passed to the function Cqrd::authorise() and subsequently to an extended version of Klein; see authorise() for details
	 */
	public static function init(array $config = array(), $routePermissions = array()){
		if(is_null(self::$_singleton)){
			self::$_singleton = new Cqrd($config);
		}
		
		$C = &self::$_singleton;
		$C->require_https();
		$C->session_start();
		
		$klein = $C->klein();
		$C->mongo();
		$C->user();
		$C->authorise($routePermissions);
		$C->phpids();
		
		$klein->endInit();
		$klein->respond($C->config('klein/responses/header'));
		
		return $C;
	}
	
	public function run(){
		$klein = $this->klein();
		$klein->respond($this->config('klein/responses/footer'));
		$klein->dispatch();
	}
	
	/**
	 * Merge data into the Klein ServiceProvider's shared data and then display the header; should be used from within a Klein request that will then call footerAndAbort() as the head is generally routed automatically.
	 */
	public function header($data = array()){
		$klein = $this->klein();
		$service = $klein->service();
		$service->sharedData()->merge($data);
		$cb = $this->config('klein/responses/header');
		$cb($klein->request(), $klein->response(), $service);
	}
	
	/**
	 * Display the footer and then cease further request handling by Klein; should be used from within a Klein request. Will set the HTTP response code accordingly.
	 */
	public function footerAndAbort($code = null){
		$klein = $this->klein();
		$cb = $this->config('klein/responses/footer');
		$cb($klein->request(), $klein->response(), $klein->service());
		$klein->abort($code);
	}
	
	/**
	 * An abbreviated convience wrapper for the singleton method. Does not take parameters as it is expected to be used after initial instantiation with init().
	 */
	public static function s(){
		return self::$_singleton;
	}
	
	/**
	 * Get or set a configuration option depending on the presence of an optional second parameter.
	 * As the configuration array is multidimensional, hierarchy is defined with /s e.g. dim1/dim2/dim3 for $_config['dim1']['dim2']['dim3']
	 */
	public function config($key = null, $val = null){
		if(empty($key)){
			return $this->_config;
		}
		$key = explode("/", $key);
		$base = array();
		$conf = &$this->_config;
		foreach($key as $dim){
			$base[] = $dim;
			if(!isset($conf[$dim])){
				throw new Exception("Cqrd configuration key ".implode("/", $base)." does not exist; an attempt was made to get / set ".implode("/", $key).".");
			}
			$conf = &$conf[$dim];
		}
		if(!is_null($val)){
			$conf = $val;
		}
		return $conf;
	}
	
	/**
	 * Use the entropy provided in the configuration to derive seeds / peppers from HMACs such that they are constant across requests
	 * Including this in derivation of keys, passphrases, password hashes etc. requires that compromise of data requires:
	 * 1. Access to at-rest, encrypted data
	 * 2. Access to application data (configuration entropy)
	 * 3. Secret key (e.g. user's password, session ID)
	 */
	public function seed($data, $algo = null, $raw = false){
		if(is_null($this->_entropy)){
			switch($this->_config['entropy']['type']){
				case "file":
					$this->_entropy = file_get_contents($this->_config['entropy']['src'], $this->_config['entropy']['file_use_include_path']==true);
					if($this->_entropy === false){
						throw new Exception("Unable to read entropy from file '{$this->_config['entropy']['src']}'.");
					}
					break;
				case "raw":
					$this->_entropy = $this->_config['entropy']['src'];
					$this->_config['entropy']['src'] = "***HIDDEN***";
					break;
			}
		}
		return hash_hmac(is_null($algo) ? $this->config('entropy/hmac_algo') : $algo, $data, $this->_entropy, $raw);
	}	
	
	/**
	 * Convenience wrapper for openssl_random_pseudo_bytes()
	 */
	public static function rand($length, $accept_weak = false){
		$strong;
		$rand = openssl_random_pseudo_bytes($length, $strong);
		if($rand===false){
		   throw new Exception("Unable to generate random bytes.");
		}
		if(!$strong && !$accept_weak){
			throw new Exception("Cryptographically weak algorithm used to generate pseudorandom bytes.");
		}
		return $rand;
	}
	
	/**
	 * Redirect if HTTPS is required
	 */
	private function require_https(){
		if($this->config('https/require')!==false && $this->config('env')!=='dev' && !$_SERVER['HTTPS']){
			$u = parse_url($_SERVER['SCRIPT_URI']);
			$url = array(
				"https://{$this->config('https/domain')}",
				$u['path']
			);
			if(isset($u['query'])){
				$url[] = '?';
				$url[] = $u['query'];
			}
			$url = implode("", $url);
			header("Location: {$url}", true, 301);
			die("Redirecting to encrypted connection");
		}
	}
	
	/**
	 * Encrypted sessions that use the session_id as the key
	 */
	private function session_start(){
		$s = $this->config('session');
		session_set_save_handler(new EncryptedFileSession($s['cipher'], $s['hmac_algo'], $this->seed('CQRD_SESSION', 'sha512', true)));
		ini_set('session.entropy_file', $s['ini']['entropy_file']);
		ini_set('session.entropy_length', $s['ini']['entropy_length']);
		ini_set('session.hash_function', $s['ini']['hash_function']);
		ini_set('session.cookie_httponly', true);
		ini_set('session.use_only_cookies', true);
		ini_set('session.cookie_secure', $this->config('env')!=='dev' && $this->config('https/require')!==false);
		ini_set('session.use_trans_sid', false);
		session_start();
		$this->_session_encrypted = true;
		$this->csrf();
	}
	
	/**
	 * Instantiate a Klein object with the custom ServiceProvider to track rendering status for XSS protection
	 */
	public function klein(){
		if(!isset($this->_klein)){
			$this->_klein = new \Oonix\Cqrd\Klein(new \Oonix\Cqrd\ServiceProvider(), null, new \Oonix\Cqrd\RouteCollection());
		}
		return $this->_klein;
	}
	
	/**
	 * Instantiate a MongoDB connection using the extended Mongo classes with automated PhpRbac integration
	 */
	public function mongo($useNamespace = true){
		$conf = $this->config('mongo');
		if(!isset($this->_mongo)){
			$this->_mongo = new \Oonix\Mongo\MongoClient($this->rbac(), array($this, 'userId'), array($this, 'mongoRbacPermPath'), $conf['conn']['server'], $conf['conn']['options'], $conf['conn']['driver_options']);
		}
		return $useNamespace ? $this->_mongo->{$conf['namespace']} : $this->_mongo;
	}
	
	public function mongoRbacPermPath($action, $obj, array $metaData = null){
		$namespace = $this->config('mongo/namespace');
		$guestAllowed = array(
			"{$namespace}/cqrd/users"
		);
		return in_array($obj->getRbacPath(), $guestAllowed) ? false : null;
	}
	
	public function user(){
		if(!isset($this->_user)){		
			try {
				$this->_user = User::authenticateBySession();
			}
			catch(UserException $e){}
			if(!isset($this->_user) && isset($_POST['CQRD']['LOGIN']) && $this->checkCSRF()){
				try {
					$this->_user = User::logIn(new String($_POST['CQRD']['LOGIN']['user']), $_POST['CQRD']['LOGIN']['password']);
				}
				catch(UserException $e){
					if($e instanceof UserSecurityException){
						throw $e;
					}
				}
			}
			else if(!is_null($this->_user) && isset($_POST['CQRD']['LOGOUT']) && $this->checkCSRF()){
				$this->_user->logOut();
				$this->_user = null;
			}
		}
		return $this->_user;
	}
	
	public function userId(){
		$user = $this->user();
		if(is_null($user)){
			return 0;
		}
		return $user->authenticatedId() ?: 0;
	}
	
	public function csrf(){
		if(!$this->_session_encrypted){
			throw new CqrdException("Session must be encrypted with Cqrd::session_start() prior to setting / getting the CSRF key.");
		}
		if(!isset($_SESSION['OONIX']['CQRD']['CSRF'])){
			$_SESSION['OONIX']['CQRD']['CSRF'] = strtr(base64_encode($this->rand($this->config('csrf/bytes'))), "+/=", "_||");
		}
		return $_SESSION['OONIX']['CQRD']['CSRF'];
	}
	
	public function checkCSRF(){
		return $this->csrf()===$_POST['CQRD']['CSRF'];
	}
	
	/**
	 * Run PHPIDS monitoring on specified data or, if null, all request globals. Only act on the outcome in the latter case, otherwise simply return the result.
	 */
	public function phpids($data = null){
		$act = false;
		if(is_null($data)){
			$act = true;
			$data = array(
				'GET' => $_GET,
				'POST' => $_POST,
				'COOKIE' => $_COOKIE
			);
		}
		
		$init = \IDS\Init::init($this->config('phpids/ini'));
		$ids = new \IDS\Monitor($init);
		$result = $ids->run($data);

		if(!$result->isEmpty()) {
			if(!$act){
				return $result;
			}
			$impact = $result->getImpact();
			$limits = $this->config('phpids/limits');
			$abort = $impact >= $limits['abort'];
			$this->log(1, ($impact >= $limits['high'] ? 'High' : 'Medium'), 'IDS', 'Page', 'View', null, ($abort ? 'Fail' : 'Success'), $result);
			if($abort){
				$C = $this;
				$this->klein()->respond(function($req, $resp, $service) use ($C, $limits) {
					$C->header(array('cqrdData' => array('view' => 'phpids_abort')));
					$service->render("{$this->config('klein/views/dir')}/{$limits['abort_view']}.phtml");
					$C->footerAndAbort(403);
				});
			}
		}
		
	}
	
	/**
	 * Instantiate and return an instance of PhpRbac
	 */
	public function rbac(){
		if(is_null($this->_rbac)){
			$this->_rbac = new \PhpRbac\Rbac();
		}
		return $this->_rbac;
	}
	
	/**
	 * Check if the current user has RBAC permission
	 */
	public function rbacCheck($perm){
		$id = $this->userId();
		if(!is_int($id) || $id<=1){ //PhpRbac uses 1 for root, enforce minimal permissions by blocking root
			return false;
		}
		return $this->rbac()->check($perm, $id);	
	}
	
	/**
	 * Create a log entry with chained HMAC authentication
	 */
	public function log(){
		var_dump(func_get_args());
	}
	
	/**
	 * The Klein class is extended to respond in the case of a particular RBAC permission being assigned to the current user.
	 * These act in the same way as Klein::respond() except a PhpRbac permission is prepended to the argument list.
	 * If a route is matched but the current session does not refer to a user with the correct permissions the login page is shown to anonymous users and a 403 to authenticated ones.
	 * 
	 * @param array $routePermissions	Array of arrays; each child array should have a PhpRbac permission as its first element and all remaining (optional) elements will be passed to Klein's route matching
	 */
	protected function authorise($routePermissions){
		$klein = $this->klein();
		$C = $this;
		
		$notPermission = function($req, $resp, $service) use ($C, $klein) {
			$user = $C->user();
			$conf = $C->config('klein/views');
			if(is_null($user)){
				$view = "{$conf['dir']}/{$conf['default']['login']}.phtml";
				$data = array('view' => 'login');
			}
			else {
				$view = "{$conf['dir']}/{$conf['default']['403']}.phtml";
				$data = array('view' => '403');
			}
			
			$C->header(array('cqrdData' => $data));
			$service->render($view);
			$C->footerAndAbort(403);
		};
		
		foreach($routePermissions as $route){
			$route[] = &$notPermission;
			call_user_func_array(array($klein, 'respondIfNotPermission'), $route);
		}
	}
	
	/**
	 * Sign a variable with an HMAC of its content. Return an array of the original value sorted (for arrays) and JSON-encoded + the HMAC or, if such an array is passed, confirm the signature.
	 * This is valuable when storing sensitive information in the database to later ensure that it was created by the app (assuming adequate protection of the entropy source).
	 * 
	 * @param mixed $val			The value for signing, or an array for confirmation.
	 * @param bool $throwOnFail		Only applipcable when checking for a valid signature; throws an exception rather than returning false. Defaults to true.
	 * @param string $key			An optional string to vary the HMAC password.
	 */
	public function hmac($val, $throwOnFail=true, $key=null){
		$hmacKey = 'CQRD_HMAC';
		$valKey = 'CQRD_VAL';
		if(is_array($val) && isset($val[$hmacKey])){
			$action = "check";
			$toCheck = $val[$hmacKey];
			$val = $val[$valKey];
		}
		else {
			$action = "sign";
			$this->hmac_prepare($val);
			$val = json_encode($val);
		}
		
		$hmac = hash_hmac($this->config('entropy/hmac_algo'), $val, $this->seed("CQRD_HMAC_PASS_{$key}", null, true));
		
		if($action=="sign"){
			return array(
				$valKey => $val,
				$hmacKey => $hmac
			);
		}
		else {
			if($hmac===$toCheck){
				return true;
			}
			else if($throwOnFail===false){
				return false;
			}
		}
		throw new CqrdException("HMAC checking failed.");
	}
	
	/**
	 * For use by CQRD::hmac() which requires objects to be sorted. Essentially a recursive ksort but can later be extended to include object serialising.
	 */
	private function hmac_prepare(&$val){
		switch(gettype($val)){
			case "array":
				ksort($val, SORT_STRING);
				foreach($val as &$v){
					$this->hmac_prepare($v);
				}
				break;
			case "boolean": case "integer": case "double": case "string":
				break;
			default:
				throw new CqrdException("Can not produce HMAC for this object type: ".gettype($val));
		}
	}
	
	/**
	 * Return the last git commit; useful as a query string appended to JS / CSS assets to ensure that they are not out of date
	 * 
	 * @param string $path			The file for which the last commit hash should be returned.
	 */
	public function lastGitCommit($path){
		if($this->config('env')==='dev'){
			return time();
		}
		$path = escapeshellarg($path);
		return `git log -n 1 --pretty=format:'%h' $path`;
	}
	
}

class_alias("\Oonix\Cqrd\Cqrd", "\Oonix\Cqrd\C");

?>
