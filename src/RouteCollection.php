<?php

namespace Oonix\Cqrd;

class RouteCollection extends \Klein\DataCollection\RouteCollection {

	/**
	 * Pop a route off the end and insert it into an arbitrary position.
	 * Allows routes to be processed in a different order to their creation.
	 */
	public function popAndSlice($pos){
		$key = array_pop(array_keys($this->attributes));
		$route = array_pop($this->attributes);
		$this->attributes[$key] = $route;
		$pos = min(count($this->attributes), max(0, (int) $pos));

		$pre = array_slice($this->attributes, 0, $pos, true);
		$post = array_slice($this->attributes, $pos, null, true);
		$pre[$key] = $route;
		$this->attributes = array_merge($pre, $post);
	}

}

?>
