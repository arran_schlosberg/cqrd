<?php

namespace Oonix\Cqrd\Types;

/**
 * Type hinting is limited in the specific types that can be specified.
 * Abstract class to allow extension of this to arbitrary criteria as defined by the $_typecheck callable.
 * The expectation is, in keeping with standard PHP behaviour, that input is flexible (e.g. String accepts all scalar values) and output is cast as expected
 * Easier than using C instead of PHP :)
 */
abstract class TypeHint {

	/**
	 * Underlying value that we are typing
	 *
	 * @var mixed
	 * @access protected.
	 */
	protected $_val = null;
	
	/**
	 * Is the initial value constant?
	 *
	 * @var bool
	 * @access protected
	 */
	protected $_const;
	
	/**
	 * Is it OK to cast this type as a string? Used by __toString()
	 *
	 * @var bool
	 * @access protected
	 */
	protected $_string_cast_ok = false;
	
	protected $_allow_null = true;
	
	/**
	 * Verify whether or not $_val is of the correct 'type'.
	 * Must return an array(bool, mixed) with success of the check, and the value cast appropriately
	 *
	 * @param mixed $val
	 * @access protected
	 * @return array
	 */
	protected abstract function typeCheckAndCast($val);
	
	public function __construct($val, $const = false){
		$this->set($val, $const);
	}
	
	public function set($val, $const = false){
		if($this->_const && !is_null($this->_val)){
			throw new TypeException("Can not set new value for constant.");
		}
		
		if($this->_allow_null && is_null($val)){
			$this->_val = null;
		}
		else {
			$typed = $this->typeCheckAndCast($val);
			if(!$typed[0]){
				throw new TypeException("Type checking failed for value '{$val}' and type '".get_class($this)."'.");
			}
			$this->_val = $typed[1];
		}
		
		$this->_const = $const;
	}
	
	public function __set($key, $val){
		$this->set($val);
	}
	
	public function get(){
		return $this->_val;
	}
	
	public function __get($key){
		return $this->get();
	}
	
	public function __toString(){
		if(!$this->_string_cast_ok){
			throw new TypeException("Can not cast value as string.");
		}
		
		return (string) $this->_val;
	}
	
	/**
	 * Use invocation as a function for a shortcut to get()
	 */
	public function __invoke(){
		return $this->get();
	}

}

?>
