<?php

namespace Oonix\Cqrd;

//force certain values to be strings to protect against MongoDB insertion attacks whereby arrays are substituted
use Oonix\Cqrd\Types\String;
use Oonix\Cqrd\Types\Bool;
use \MongoId;
use \MongoCursor;
use CqrdException as Exception;
use Oonix\Encryption\EncUtils;

class User {
	/**
	 * MongoDB users collection within the namespace defined by Cqrd configuration
	 */
	protected static $_users = null;
	
	/**
	 * Copy of the Cqrd configuration directives relevant to users
	 */
	protected static $_conf = null;
	
	/**
	 * Details of the current user, not necessarily authenticated though.
	 */
	private $_me;
	
	/**
	 * Assuming authentication was performed by session, which index in the Mongo document's array?
	 */
	private $_sess_idx;
	
	/**
	 * Is the current user actually authenticated?
	 */
	private $_authenticated = false;
	
	/**
	 * Can only be set when authenticating by password; gives us the option to decrypt various user data at this time.
	 */
	private $_passphrase;
	
	/**
	 * Valid actions to be performed with data stored with ::pkiDrop() - this is passed as the 'action' index of the array.
	 * See ::pkiCollect() for their implementations.
	 */
	private $_pkiActions = array('key');

	/**
	 * Find a user by _id or instantiate with a MongoCursor that has already found the document (used for authentication by session)
	 */
	public function __construct(String $user, MongoCursor $cursor = null, $verify_session = false){
		if(is_null($cursor)){
			$this->_me = self::users()->findOne(["_id" => (string) $user]);
			if(empty($this->_me)){
				throw new UserException("User '{$user}' does not exist.");
			}
		}
		else {
			if($cursor->count(true)!==1){
				throw new UserException("The session cursor for creating a User must have count of 1.");
			}
			$cursor = $cursor->fields(array());
			$cursor->reset();
			$this->_me = $cursor->getNext();
		}
		$this->_me['password']['salt'] = base64_decode($this->_me['password']['salt']);
		
		if($verify_session){
			$this->_authenticated = $this->verifySession();
			if(!$this->_authenticated){
				throw new UserException("Invalid session key for the ID.");
			}
		}
	}
	
	public function authenticateByPassword($password){
		$data = &$this->_me['password'];
		$this->_passphrase = $this->passphrase($password, $data['salt'], $data['iterations'], $data['algo'], true);
		return $data['hash']===$this->passhash($this->_me['_id'], $this->_passphrase) ? $this->setAuthStatus(new Bool(true)) : false;
	}
	
	/**
	 * Establish a MongoCursor that matches the MongoId and hashed key, this is passed to the constructor which will optionally verify the authenticity of the key.
	 */
	public static function authenticateBySession(){
		if(!isset($_SESSION['OONIX']['CQRD']['USER'])){
			return null;
		}
		$sess = $_SESSION['OONIX']['CQRD']['USER'];
		$sess = ['id' => new MongoId($sess['id']), 'key' => (string) $sess['key']];
		return new User(new String(null), self::users()->find(['sessions' => ['$elemMatch' => $sess]]), true);
	}
	
	public function verifySession(){
		foreach($this->_me['sessions'] as $idx => $sess){
			if($sess['id']==$_SESSION['OONIX']['CQRD']['USER']['id']){ //do not use strict === comparison as they are different objects but with the same ID
				if($sess['key']===$_SESSION['OONIX']['CQRD']['USER']['key'] && $sess['key']===$this->sessionKey($sess['id'])){
					$this->_sess_idx = $idx;
					return true;
				}
				$this->_sess_idx = null;
				return false;
			}
		}
	}

	public static function logIn(String $user, $password){
		$u = new User($user);
		if(!$u->authenticateByPassword($password)){
			throw new UserException("Incorrect password.");
		}
		$u = $u->pkiCollect()->newSession();
		if(isset($_POST['CQRD']['LOGIN']['UNLOCK']) && is_array($_POST['CQRD']['LOGIN']['UNLOCK'])){
			foreach($_POST['CQRD']['LOGIN']['UNLOCK'] as $refHmac){
				$u->unlockKey($refHmac);
			}
		}
		return $u->clearPassPhrase();
	}
	
	public function logOut(){
		self::users()->update(["_id" => $this->_me['_id']], ['$pull' => ['sessions' => ['id' => $_SESSION['OONIX']['CQRD']['USER']['id']]]]);
		$this->_authenticated = $this->_sess_idx = null;
		unset($_SESSION['OONIX']['CQRD']['USER']);
	}
	
	public function newSession(){
		$id = new MongoId();
		$key = $this->sessionKey($id);
		$sess = ['id' => $id, 'key' => $key];
		self::users()->update(["_id" => $this->_me['_id']], ['$push' => ['sessions' => $sess]]);
		$this->_me['sessions'][] = $sess;
		$this->_sess_idx = count($this->_me['sessions']) - 1;
		$_SESSION['OONIX']['CQRD']['USER'] = $sess;
		return $this;
	}
	
	/**
	 * Allow any data to be cryptographically sent to a user for "collection" upon next authentication, at which point their private key is temporarily unlocked.
	 * 
	 * @param array $plain_text		Must contain a valid 'action' along with 'data'. Actions are then performed in ::pkiCollect()
	 */
	public function pkiDrop($plain_text){
		if(!is_array($plain_text)){
			throw new UserPkiException("Can not drop a non-array. Action and data are both required.");
		}
		if(!isset($plain_text['action']) || !in_array($plain_text['action'], $this->_pkiActions)){
			throw new UserPkiException("A valid action must be specified for pkiDrop(). Currently implemented actions are: ".implode(", ", $this->_pkiActions));
		}
		if(!isset($plain_text['data'])){
			throw new UserPkiException("No data was passed to pkiDrop().");
		}
		$conf = $this->conf()['pki']['symmetric'];
		$key = Cqrd::rand($conf['cipher_key_len']);
		$hmacKey = Cqrd::rand($conf['hmac_key_len']);
		$cipher_text = $this->EncUtil($key, $hmacKey)->encrypt(json_encode($plain_text));
		openssl_public_encrypt($key.$hmacKey, $locked_keys, openssl_pkey_get_public($this->_me['pki']['pub']));
		self::users()->update(["_id" => $this->_me['_id']], ['$push' => ['inbox' => array('id' => new MongoId(), 0 => base64_encode($locked_keys), 1 => $cipher_text)]]);
		return $this;
	}
	
	public function pkiCollect(){
		$priv = openssl_pkey_get_private($this->_me['pki']['priv'], $this->_passphrase);
		$conf = $this->conf()['pki']['symmetric'];
		foreach($this->_me['inbox'] as $drop){
			openssl_private_decrypt(base64_decode($drop[0]), $locked_keys, $priv);
			$key = substr($locked_keys, 0, $conf['cipher_key_len']);
			$hmacKey = substr($locked_keys, $conf['cipher_key_len'], $conf['hmac_key_len']);
			$plain_text = json_decode($this->EncUtil($key, $hmacKey)->decrypt($drop[1]), true);
			switch($plain_text['action']){
				case 'key':
					$ref = $this->keyRefHash($plain_text['data']['ref']);
					if(isset($ref, $this->_me['data']['keys']) && $plain_text['data']['overwrite']!==true){
						break;
					}
					else { //yes there is a break but I'd rather be certain
						$toLock = $plain_text['data'];
						unset($toLock['overwrite']);
						$locked = $this->EncUtil($this->_passphrase)->encrypt(json_encode($toLock));
						self::users()->update(["_id" => $this->_me['_id']], ['$set' => ["data.keys.{$ref}" => $locked]]);
					}
					break;
				default:
					openssl_pkey_free($priv);
					throw new UserPkiException("The PKI action '{$plain_text['action']}' has not yet been implemented.");
			}
			self::users()->update(["_id" => $this->_me['_id']], ['$pull' => ["inbox" => ['id' => $drop['id']]]]);
		}
		openssl_pkey_free($priv);
		return $this;
	}
	
	public function giveKey($ref, $key, $overwrite = true){
		$this->pkiDrop(array(
			'action' => 'key',
			'data' => array('ref' => $ref, 'key' => $key, 'overwrite' => $overwrite===true)
		));
		return $this;
	}
	
	public function unlockKey($refHmac){
		list($ref, $hmac) = explode("|", $refHmac);
		if(self::unlockHmac($ref)!==$refHmac){
			throw new UserSecurityException("Attempt to unlock key '{$ref}' with invalid HMAC.");
		}
		$hash = $this->keyRefHash($ref);
		if(isset($this->_me['data']['keys'][$hash])){
			$unlocked = json_decode($this->EncUtil($this->_passphrase)->decrypt($this->_me['data']['keys'][$hash]), true);
			$_SESSION['OONIX']['CQRD']['USER']['KEYS'][$ref] = $unlocked['key'];
		}
		return $this;
	}
	
	public static function unlockHmac($ref){
		$C = Cqrd::s();
		return "{$ref}|".$C->seed("USER_KEY_UNLOCK|{$ref}|".$C->csrf());
	}
	
	public function keyRefHash($ref){
		return preg_replace("/[^a-z0-9]/i", "", base64_encode(hash_hmac('sha512', $ref, $this->hmacKeyXor($this->_passphrase, 'USER_KEY_REF_XOR'), true)));
	}
	
	private function EncUtil($key, $hmacKey = null){
		$conf = $this->conf()['pki']['symmetric'];
		/*
		 * The HMAC key used for cipher text authentication should be different to the cipher key.
		 * However we will XOR with random data derived from CQRD entropy, ensuring that the two keys are different.
		 * The cipher key is included to add additional secrecy should the app entropy be compromised.
		 */
		if(is_null($hmacKey)){
			$hmacKey = $key;
		}
		return new EncUtils($key, $conf['cipher'], 0, false, $conf['hmac'], $this->hmacKeyXor($hmacKey));
	}
	
	/**
	 * Certain values in the database are hashed with a secret key, e.g.:
	 * PKI drops are authenticated with an HMAC, the key of which is randomly generated along with the symmetric key.
	 * 
	 * To ensure that such values are not maliciously injected directly into the database, the HMAC key is XORd with a seed generated by CQRD app entropy.
	 */
	public function hmacKeyXor($hmacKey, $seed = 'USER_HMAC_KEY_XOR'){
		return $hmacKey ^ substr(Cqrd::s()->seed($seed, 'sha512', true), 0, strlen($hmacKey));
	}
	
	public function sessions(){
		return $this->_me['sessions'];
	}
	
	/**
	 * Tie the session key to the user.
	 */
	public function sessionKey(MongoId $id){
		return hash_hmac(self::algo(), "{$id}_{$this->_me['_id']}", self::saltAndPepper($this->_me['password']['salt']));
	}
	
	public function clearPassPhrase(){
		$this->_passphrase = null;
		return $this;
	}
	
	private function setAuthStatus(Bool $status){
		$this->_authenticated = (bool) $status;
		return $this;
	}

	/**
	 * Lazy instantiation of self::$_users
	 */
	protected static function users(){
		if(is_null(self::$_users)){
			self::$_users = Cqrd::s()->mongo()->cqrd->users;
		}
		return self::$_users;
	}
	
	/**
	 * Lazy instantiation of self::$_conf
	 */
	protected static function conf(){
		if(is_null(self::$_conf)){
			self::$_conf = Cqrd::s()->config('users');
		}
		return self::$_conf;
	}

	/**
	 * Create a user
	 *
	 * Must meet the following conditions:
	 * - password sufficiently strong as defined in Cqrd::config('users/password/strength')
	 * - user does not already exist
	 *
	 * Additionally:
	 * - derive a secret passphrase from ther user's password with PBKDF2
	 * - store the HMAC of the username with the passphrase as the key, for password authentication
	 * - create a private / public key pair with the former encrypted with the passphrase
	 * - user documents have data and inbox arrays; the former contains symmetrically encrypted data (passphrase as the key) and the latter pubkey encrypted data
	 *   - the symmetric data can be keys for things such as MongoDB collections
	 *   - the inbox can be for messaging OR to "drop" data to be transferred to the symmetric section, such as providing a user with access to a new collection
	 *   - note that these can only be accessed when the user first logs in as they require the password to derive the passphrase - as sessions are encrypted we can temporarily store a copy in them
	 */
	public static function create(String $user, $password){
		self::checkPasswordStrength($password);
		if(self::userExists($user)){
			throw new UserExistsException($user);
		}
		
		$conf = self::conf();
		
		$salt = Cqrd::rand($conf['password']['hash']['salt_len']);
		$passphrase = self::passphrase($password, $salt);
		$hash = self::passhash($user, $passphrase);
		
		$res = openssl_pkey_new($conf['pki']);
		openssl_pkey_export($res, $privKey, $passphrase);
		$pubKey = openssl_pkey_get_details($res)['key'];
		openssl_pkey_free($res);
		
		self::users()->insert([
			"_id" => (string) $user, //enforce unique username
			"objId" => new MongoId(), //is needed to create an integer ID with User::id()
			"password" => [
				"algo" => $conf['password']['hash']['algo'],
				"iterations" => $conf['password']['hash']['min_iterations'],
				"salt" => base64_encode($salt),
				"hash" => $hash
			],
			"pki" => [
				"pub" => $pubKey,
				"priv" => $privKey //requires the user's password to derive the passphrase
			],
			"sessions" => [],
			"inbox" => [], //repository for messages encrypted with the public key
			"data" => [
				"keys" => (object)[] //casting as an object inserts a JSON {} instead of []
			] //repository of encrypted data, e.g. mongo collection-specific encryption keys
		]);
	}
	
	public static function userExists(String $user){
		return self::users()->count(["_id" => (string) $user]);
	}
	
	/**
	 * Check that a password meets requirements defined in Cqrd::config('users/password/strength')
	 * Both a regular expression and a callback can be required.
	 */
	public static function checkPasswordStrength($password, $throw_exception = true){
		$conf = self::conf()['password']['strength'];
		
		$pass = true;
		if(!empty($conf['regex'])){
			$pass = preg_match($conf['regex'], $password);
		}
		
		if(isset($conf['callback']) && is_callable($conf['callback'])){
			$pass = $pass && ((bool) call_user_func($conf['callback'], $password));
		}
		
		if(!$pass && $throw_exception){
			throw new WeakPasswordException($conf['desc']);
		}
		return $pass;
	}
	
	/**
	 * See User::create() for uses of the passphrase
	 * 
	 * @param bool $forceIterations	Allows iterations to be less than min_iterations specified in config, useful when increasing min_iterations and checking an old user.
	 */
	public static function passphrase($password, $salt, $iterations = null, $algo = null, $forceIterations = false){
		$conf = self::conf()['password']['hash'];
		$algo = self::algo($algo);
		$iterations = is_null($iterations) ? $conf['min_iterations'] : max($iterations, $conf['min_iterations']);
		$salt = self::saltAndPepper($salt, $algo);
		return hash_pbkdf2($algo, $password, $salt, $iterations, 0, true);
	}
	
	public static function saltAndPepper($salt, $algo = null){
		return hash_hmac(self::algo($algo), Cqrd::s()->seed("CQRD_USER_SALT"), $salt, true);
	}
	
	public static function algo($algo = null){
		return is_null($algo) ? self::conf()['password']['hash']['algo'] : $algo;
	}

	/**
	 * Compute the hash stored in the user document for authentication.
	 * This is done from either the raw password or the passphrase derived by User::passphrase(), the former is performed if a salt is provided.
	 */
	public static function passhash($user, $pass, $salt = null, $iterations = null, $algo = null){
		if(is_null($salt)){ //assume that $pass is derived from ::passphrase()
			$passphrase = $pass;
		}
		else {
			$passphrase = self::passphrase($pass, $salt, $iterations, $algo);
		}
		$algo = self::conf()['password']['hash']['algo'];
		return hash_hmac($algo, $user, $passphrase);
	}
	
	public function isAuthenticated(){
		return $this->_authenticated;
	}
	
	/**
	 * Return a unique integer ID that must be positive when stored in an 8 byte signed int (SQLITE 3); therefore use 63 bits
	 */
	public function id(){
		$o = $this->_me['objId'];
		$u = $this->_me['_id'];

		/**
		 * Sources of data to be used in the array along with how many bits to use (least significant).
		 * Ordered within the array from most to least significant bits in the final ID. Done to maximise the probability of increasing values and guarantee them on a single process.
		 * The number of bits have been chosen to minimise the probability of a collision in the $userHashBits of unique usernames' hashes (remember the birthday paradox!):
		 * - using the full timestamp means that collisions must occur within the same second (before 2038 of course; then the world will end)
		 * - the incrementing value reduces the window for collisions by dividing each second further, 10 bits is roughly a process-millisecond
		 * - any project using Cqrd that uses Mongo sharding is large enough to warrant increasing the bits allocated to process / host; so keep these minimal by default as they are low entropy
		 * - fill all remaining bits from the hash of the username
		 */
		$userHashBits = 11; //needed for both the number of bits + the substring from the end of the hash
		$parts = array(
			array($o->getTimeStamp(), 32),
			array($o->getInc(), 16),
			array($o->getPid(), 2),
			array(base_convert(substr($o, 8, 6), 16, 10), 2), //hash of the hostname; no function provided by class?
			array(base_convert(substr(md5($u), -ceil($userHashBits / 4)), 16, 10), $userHashBits)
		);
		
		$totalBits = 0;
		$id = 0;
		foreach($parts as $p){
			$totalBits += ($bits = $p[1]);
			$mask = (1 << $bits) - 1;
			$use = $p[0] & $mask;
			$id = ($id << $bits) + $use;
		}

		if($totalBits!=63){
			throw new UserException("Use exactly 63 bits when calculating User integer ID. Currently using {$totalBits}.");
		}

		return $id;
	}
	
	public function authenticatedId(){
		return $this->isAuthenticated() ? $this->id() : false;
	}

}

?>
