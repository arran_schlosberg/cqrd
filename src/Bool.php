<?php

namespace Oonix\Cqrd\Types;

class Bool extends TypeHint {

	protected $_string_cast_ok = true;
	
	protected function typeCheckAndCast($val){
		return array(
			is_bool($val) || is_numeric($val) || is_array($val),
			is_array($val) ? count($val)>0 : (bool) $val
		);
	}

}

class_alias("\Oonix\Cqrd\Types\Bool", "\Oonix\Cqrd\Types\Boolean");

?>
